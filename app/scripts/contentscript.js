import {
  cond,
  defaultTo,
  drop,
  length,
  nth,
  pathSatisfies,
  pipe,
  propEq
} from "ramda";
import browser from "webextension-polyfill";
import { withoutFilteredMessages } from "./scripts/filters";
import {
  addCurtain,
  clickNewMessage,
  hasNewMessages,
  removeCurtain,
  spanWithText,
} from "./scripts/utils.js";

const NEXT_MATCH_TIMEOUT = 2000;
const DELETE_FLOW_TIMEOUT = 1000;

const scrollToBottom = callback => {
  const div = document.querySelector(".messageList");
  div.scrollTop = div.scrollHeight - div.clientHeight;
  return setTimeout(() => callback(), 1000);
};
const safeLength = pipe(defaultTo([]), length);
const nextConversation = () => {
  const matches = drop(2, [...document.querySelectorAll(".matchListItem")]);
  const allConversations = withoutFilteredMessages([
    ...document.querySelectorAll(".messageListItem__message")
  ]).reverse();
  const unrepliedConversations = allConversations
    .filter(message => message.children.length !== 1)
    .filter(message => message.querySelector("svg") === null);
  const targets = {
    matches,
    unrepliedConversations,
    allConversations
  };
  return targets;
};
let totalContactsInView = 0;
const nextAction = () => {
  addCurtain();
  totalContactsInView = safeLength([
    ...document.querySelectorAll(".messageListItem")
  ]);

  setTimeout(() => {
    if (hasNewMessages()) {
      clickNewMessage();
      return false;
    }
    let nextTarget = nextConversation();
    let nextTargetList = [
      ...nextTarget.matches,
      ...nextTarget.unrepliedConversations,
      ...nextTarget.allConversations
    ];
    if (!!nextTargetList.length) {
      removeCurtain();
      let target = nextTargetList.shift();
      target.click();
    } else {
      scrollToBottom(() => {
        if (
          [...document.querySelectorAll(".messageListItem")].length >
          totalContactsInView
        ) {
          scrollToBottom(() => nextAction());
        } else {
          removeCurtain();
          if (nextTarget.allConversations) nextTarget.allConversations.click();
        }
      });
    }
  }, NEXT_MATCH_TIMEOUT);
};

let deleteMatch = () => {
  [
    () =>
      [
        ...document.getElementsByTagName("span"),
        ...document.getElementsByTagName("button")
      ]
        .find(e => e.innerText.toLowerCase().includes("unmatch"))
        .click(),
    () =>
      [
        ...document.getElementsByTagName("span"),
        ...document.getElementsByTagName("button")
      ]
        .find(e => e.innerText.toLowerCase().includes("unmatch"))
        .click()
  ].forEach((func, i) => setTimeout(() => func(), i * DELETE_FLOW_TIMEOUT));
};

let sendInstagram = () => {
  return [
    () => {
      return nth(1, [
        ...document.querySelectorAll(".chat div div div button")
      ]).click();
    },

    () =>
      [...document.querySelectorAll('img[alt="instagram"]')]
        .find(
          pathSatisfies(str => !str.includes("Instagram"), [
            "nextSibling",
            "textContent"
          ])
        )
        .parentNode.click(),
    () =>
      [...document.querySelectorAll("span")]
        .find(e => e.textContent === "Send")
        .click(),
    nextAction
  ].forEach((func, i) => setTimeout(() => func(), i * DELETE_FLOW_TIMEOUT));
};

let sendEmoji = emojiName => () => {
  return [
    () =>
      [...document.querySelectorAll("button")]
        .find(el => el.title === emojiName)
        .click(),
    () =>
      [...document.querySelectorAll("span")]
        .find(e => e.textContent === "Send")
        .click(),
    nextAction
  ].forEach((func, i) => setTimeout(() => func(), i * 500));
};

let dislike = () => spanWithText("Nope").click();
let like = () => spanWithText("Like").click();

const commandEq = propEq("command");

const handleCommand = cond([
  [commandEq("next-action"), nextAction],
  [commandEq("delete-match"), deleteMatch],
  [commandEq("send-instagram"), sendInstagram],
  [commandEq("send-emoji-wave"), sendEmoji("waving hand")],
  [commandEq("like-button"), like],
  [commandEq("dislike-button"), dislike]
]);

browser.runtime.onMessage.addListener(handleCommand);
