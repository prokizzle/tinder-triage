import {
  allPass,
  any,
  anyPass,
  equals,
  flip,
  last,
  length,
  pipe,
  prop,
  reject,
  split,
  test
} from "ramda";

const phoneNumberRegex = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/g;

const getThreadId = pipe(
  prop("parentElement"),
  prop("parentElement"),
  prop("href"),
  split("/"),
  last
);

const contactCardRegex = /\bcontact card\b/;

const hasOneChild = pipe(prop("children"), length, equals(1));

const hasPhoneNumber = test(phoneNumberRegex);

const hasContactCard = pipe(prop("innerHTML"), test(contactCardRegex));

const hasOneChildAndNumber = allPass([hasOneChild, hasPhoneNumber]);

const hasReplyElementAndPhoneNumber = pipe(prop("textContent"), hasPhoneNumber);

const matchingPhrases = [/Text me/, /sushi/];

const hasMatchingPhrase = pipe(prop("textContent"), str =>
  any(flip(test)(str), matchingPhrases)
);

const ignoredIds = [
  "612b016247f1c701009b6f98645270388b6d5901009a08c2",
  "643c9ab8728b960100578900645270388b6d5901009a08c2"
];

const hasIgnoredId = msg => {
  const id = getThreadId(msg);
  return ignoredIds.includes(id);
};

const containsFilteredCriteria = anyPass([
  hasReplyElementAndPhoneNumber,
  hasContactCard,
  hasOneChildAndNumber,
  hasMatchingPhrase,
  hasIgnoredId
]);

export const withoutFilteredMessages = reject(containsFilteredCriteria);
