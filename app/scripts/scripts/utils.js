import { compose, curry, flatten } from "ramda";

const elementWithText = curry((element, text) =>
  [...document.querySelectorAll(element)].find(div =>
    div.innerText.includes(text)
  )
);

const elementsWithText = curry((elements, text) =>
  flatten([
    ...elements.map(el => [...document.getElementsByTagName(el)])
  ]).find(el => el.innerText.toLowerCase().includes(text.toLowerCase()))
);

export const divWithText = elementWithText("div");
export const spanWithText = elementWithText("span");
export const buttonWithText = elementWithText("button");
export const itemWithText = elementsWithText(["button", "span"]);

export const removeCurtain = () =>
  ([...document.getElementsByTagName("body")][0].style.opacity = "1");
export const addCurtain = () =>
  ([...document.getElementsByTagName("body")][0].style.opacity = "0.4");
export const hasNewMessages = () =>
  document.querySelector(".messageListItem--isNew") !== null;
export const clickNewMessage = compose(removeCurtain, () =>
  document.querySelector(".messageListItem--isNew").click()
);
