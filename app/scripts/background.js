import { propOr } from "ramda";

chrome.runtime.onInstalled.addListener(details => {
  console.log("previousVersion", details.previousVersion);
});

console.log("'Allo 'Allo! Event Page");

let store = {};

const isSnoozed = ({ matchId }) => {
  const currentTime = new Date().getTime();
  const expireAt = propOr(currentTime, matchId, store);
  if (currentTime >= expireAt) {
    delete store[matchId];
    return false;
  } else {
    return true;
  }
};

const isExpired = entry => new Date().getTime() > entry.expireAt;

const handleMessages = ({ command, ...request }, sender, sendResponse) => {
  console.log("Background command", { command, request, sender });
  if (request.command === "getSnoozed") {
    const userId = request.matchId;
    sendResponse({ userId, store });
  } else if (command === "snooze") {
    const matchId = request.matchId;
    snoozeMatch({ matchId });
    sendResponse({ message: "Snooze successful", matchId });
  }
};

let snoozeMatch = ({ matchId }) => {
  store[matchId] = new Date().getTime() + 86400;
  return store;
};

chrome.runtime.onMessage.addListener(handleMessages);

chrome.commands.onCommand.addListener(command => {
  console.log(command);
  if (!!command) {
    let queryOptions = { active: true, currentWindow: true };
    chrome.tabs.query(queryOptions, tabs => {
      const [tab] = tabs;
      chrome.tabs.sendMessage(tab.id, { command });
    });
  }
});

chrome.browserAction.onClicked(tab => {
  return chrome.tabs.sendMessage(tab.id, { action: "nextAction" });
});
